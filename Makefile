all: script

FILES = \
	g-script-js.c \
	g-script-js.h \
	script.c \
	$(NULL)

PKGS = gtk+-3.0 gjs-1.0 gjs-gi-1.0

script: $(FILES)
	$(CC) -o $@ -g -Wall -Werror $(FILES) $(shell pkg-config --cflags --libs $(PKGS))

clean:
	rm -f script
