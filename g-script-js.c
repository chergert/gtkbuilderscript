/* g-script-js.c
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gjs/gjs-module.h>
#include <gjs/gi/closure.h>
#include <gjs/gi/value.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>

#include "g-script-js.h"

struct _GScriptJsPrivate
{
	GjsContext *context;
};

enum
{
	PROP_0,
	PROP_SCRIPT,
	PROP_LAST
};

static GParamSpec *gParamSpecs[PROP_LAST];

static void gtk_buildable_init (GtkBuildableIface *);

G_DEFINE_TYPE_WITH_CODE(GScriptJs, g_script_js, G_TYPE_OBJECT,
                        G_IMPLEMENT_INTERFACE(GTK_TYPE_BUILDABLE,
                                              gtk_buildable_init))

GClosure *
g_script_js_get_function (GScriptJs   *js,
                          const gchar *function)
{
	GScriptJsPrivate *priv;
	JSContext *context;
	JSObject *callable = NULL;
	GClosure *closure;
	JSObject *global;
	JSBool jb;
	jsval v = { 0 };

	g_return_val_if_fail(G_IS_SCRIPT_JS(js), NULL);

	priv = js->priv;

	context = gjs_context_get_native_context(priv->context);
	g_assert(context);

	JS_BeginRequest(context);

	global = gjs_get_import_global(context);
	g_assert(global);

	jb = JS_GetProperty(context, global, function, &v);
	g_assert_cmpint(jb, ==, JS_TRUE);
	g_assert(JSVAL_IS_OBJECT(v));
	g_assert(!JSVAL_IS_NULL(v));
	g_assert(!JSVAL_IS_VOID(v));

	callable = JSVAL_TO_OBJECT(v);
	g_assert(callable);
	g_assert(JS_ObjectIsFunction(context, callable));

	closure = gjs_closure_new_marshaled(context, callable, function);
	g_assert(closure);
	g_assert(gjs_closure_is_valid(closure));

	JS_EndRequest(context);

	return closure;
}

/**
 * g_script_js_finalize:
 * @object: (in): A #GScriptJs.
 *
 * Finalizer for a #GScriptJs instance.  Frees any resources held by
 * the instance.
 *
 * Returns: None.
 * Side effects: None.
 */
static void
g_script_js_finalize (GObject *object)
{
	GScriptJsPrivate *priv = G_SCRIPT_JS(object)->priv;

	g_clear_object(&priv->context);

	G_OBJECT_CLASS(g_script_js_parent_class)->finalize(object);
}

/**
 * g_script_js_set_property:
 * @object: (in): A #GObject.
 * @prop_id: (in): The property identifier.
 * @value: (in): The given property.
 * @pspec: (in): A #ParamSpec.
 *
 * Set a given #GObject property.
 */
static void
g_script_js_set_property (GObject      *object,
                          guint         prop_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
	GScriptJs *js = G_SCRIPT_JS(object);

	switch (prop_id) {
	case PROP_SCRIPT: {
		const gchar *script = g_value_get_string(value);
		GError *error = NULL;

		if (script) {
			if (!gjs_context_eval(js->priv->context, script, strlen(script),
			                      "__gtk_builder__", NULL, &error)) {
				g_critical("%s", error->message);
				g_error_free(error);
			}
		}
		break;
	}
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
	}
}

/**
 * g_script_js_class_init:
 * @klass: (in): A #GScriptJsClass.
 *
 * Initializes the #GScriptJsClass and prepares the vtable.
 *
 * Returns: None.
 * Side effects: None.
 */
static void
g_script_js_class_init (GScriptJsClass *klass)
{
	GObjectClass *object_class;

	object_class = G_OBJECT_CLASS(klass);
	object_class->finalize = g_script_js_finalize;
	object_class->set_property = g_script_js_set_property;
	g_type_class_add_private(object_class, sizeof(GScriptJsPrivate));

	gParamSpecs[PROP_SCRIPT] =
		g_param_spec_string("script",
		                    _("Script"),
		                    _("Add script to evaluate."),
		                    NULL,
		                    G_PARAM_WRITABLE);
	g_object_class_install_property(object_class, PROP_SCRIPT,
	                                gParamSpecs[PROP_SCRIPT]);
}

/**
 * g_script_js_init:
 * @: (in): A #GScriptJs.
 *
 * Initializes the newly created #GScriptJs instance.
 *
 * Returns: None.
 * Side effects: None.
 */
static void
g_script_js_init (GScriptJs *js)
{
	js->priv =
		G_TYPE_INSTANCE_GET_PRIVATE(js,
		                            G_TYPE_SCRIPT_JS,
		                            GScriptJsPrivate);

	js->priv->context = gjs_context_new();
}

static void
gtk_buildable_init (GtkBuildableIface *iface)
{
}
