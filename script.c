#include <gtk/gtk.h>
#include <gjs/gjs-module.h>

#include "g-script-js.h"

static void
connect_func (GtkBuilder    *builder,
			  GObject       *object,
			  const gchar   *signal_name,
			  const gchar   *handler_name,
			  GObject       *connect_object,
			  GConnectFlags  flags,
			  gpointer       user_data)
{
	const gchar *script_name = user_data;
	GScriptJs *script;
	GClosure *closure;

	g_return_if_fail(script_name != NULL);
	g_return_if_fail(connect_object == NULL); /* TODO */

	script = G_SCRIPT_JS(gtk_builder_get_object(builder, script_name));
	if (!script) {
		g_critical("Cannot locate script %s", script_name);
		return;
	}

	closure = g_script_js_get_function(script, handler_name);
	if (!closure) {
		g_critical("Cannot locate function %s", handler_name);
		return;
	}

	g_signal_connect_closure(object, signal_name, closure,
	                         !!(flags & G_CONNECT_AFTER));
}

gint
main (gint   argc,
	  gchar *argv[])
{
	GtkBuilder *builder;
	GtkWidget *window;
	GError *error = NULL;

	gtk_init(&argc, &argv);

	/* linker hack */
	g_debug("Registering type: %s", g_type_name(G_TYPE_SCRIPT_JS));

	builder = gtk_builder_new();
	gtk_builder_add_from_file(builder, "script.ui", &error);
	gtk_builder_connect_signals_full(builder, connect_func, "script");
	g_assert_no_error(error);

	window = GTK_WIDGET(gtk_builder_get_object(builder, "window"));
	g_assert(window);

	gtk_window_present(GTK_WINDOW(window));
	gtk_main();

	return 0;
}
